#!/bin/bash

# Установочный скрипт ALFI (от Arch Linux Full Install - Полная установка Arch Linux)
# Цель скрипта - быстрое развертывание системы на BTRFS

#Скрипт создан на основе скрипта Алексея Бойко(пидора)
#Третья часть скрипта пожелаю. Чтобы все работало нужно запустить только 2 скрипта.
#Переменные
country=RU
hd_partion_ext4_home= #Дом (EXT4)
hd_partion_ext4_root= #Корень (EXT4) 
hd_partion_btrfs= #Раздел под систему (BTRFS)
swap_partion= #Раздел под свап.
plus_package='git bash-completion ntfs-3g os-prober mtools fuse ufw accountsservice wget elinks mc'  #Пишем сюда пакеты если нужно помимо базы (доп. пакеты для установки)
#-------------
echo 'Скрипт сделан на основе чеклиста Бойко Алексея(пидора) по Установке ArchLinux'
echo -e "\e[1;31mВНИМАНИЕ!!!\033[0m"
echo "Вы точно хотите продолжить установку Arch Linux?"
read -p "1 - Да , 0 - Нет : " installation_question
        if [[ $installation_question == 1 ]]; then
        echo 'Настриваем локаль'
        elif [[ $installation_question == 0 ]]; then
        exit 0
        fi
loadkeys ru
setfont cyr-sun16

echo 'Синхронизация системных часов'
timedatectl set-ntp true

echo 'Прописываем DNS'
echo 'nameserver 8.8.8.8
nameserver 1.1.1.1' > /etc/resolv.conf

echo 'Проверка соединения'
ping -c 5 ya.ru

echo 'Ваша разметка диска'
fdisk -l

echo 'На какую ФС ставим ArchLinux?'
read -p "1 - EXT4, 0 - BTRFS: " fs_setting
if [[ $fs_setting == 1 ]]; then
echo 'Форматирование разделов'
mkfs.ext4 $hd_partion_ext4_root -L root
mkfs.ext4 $hd_partion_ext4_home -L home
echo 'Монтирование разделов'
mount $hd_partion_ext4_root /mnt #Корень будущий системы
mkdir /mnt/home/
mount $hd_partion_ext4_home /mnt/home #Дом будущий системы
elif [[ $fs_setting == 0 ]]; then
echo 'Форматирование разделов'
mkfs.btrfs -f $hd_partion_btrfs
echo 'Монтирование подтомов и создание подтомов'
mount $hd_partion_btrfs /mnt
btrfs subvolume create /mnt/root
btrfs subvolume create /mnt/home 
btrfs subvolume create /mnt/pkg
btrfs subvolume create /mnt/snapshots
umount /mnt
mount -o subvol=root,compress=lzo,autodefrag $hd_partion_btrfs /mnt
mkdir /mnt/home
mkdir -p /mnt/var/cache/pacman/pkg
mkdir /mnt/snapshots
mount -o subvol=home,compress=lzo,autodefrag $hd_partion_btrfs /mnt/home
mount -o subvol=pkg,compress=lzo,autodefrag $hd_partion_btrfs /mnt/var/cache/pacman/pkg
mount -o subvol=snapshots,compress=lzo,autodefrag $hd_partion_btrfs /mnt/snapshots
echo 'Создаем папки для снапшотов'
mkdir /mnt/snapshots/{pacsnap,daily}/ #daily-каждый день по таймеру:pacsnap-перед обновление системы.
fi

echo 'Подключаем Swap-раздел?'
read -p "1 - Да, 0 - Нет: " swap_setting
if [[ $swap_setting == 1 ]]; then
mkswap $swap_partion -L swap
swapon $swap_partion
elif [[ $swap_setting == 0 ]]; then
read -p " Swap-раздел не подключен" -t 3
fi

echo 'Выбор зеркал для загрузки. Ставим Российские зеркала'
echo 'Для этого ставим утилиту'
wget https://www.dropbox.com/s/xa0kv7eggxp0lbp/fetchmirrors.pkg.tar.xz
pacman -Syy pacman-contrib && pacman -U ~/fetchmirrors.pkg.tar.xz
fetchmirrors -q -c "$country"

echo 'Синхранизация репозиторий'
pacman -Syy

echo 'Установка основных пакетов'
pacstrap /mnt base base-devel "$plus_package"

echo 'Настройка fstab'
genfstab -pU /mnt >> /mnt/etc/fstab
echo 'Копируем вторую часть конфига в новую систему'
cp arch_2.sh /mnt/
cp arch_3.sh /mnt/

echo 'Настраиваем зеркала в целевой системе'
cp -p /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/ #Это сделано для того, чтобы fetchmirrors не тащить(устанавливать) в целевую систему
echo 'После chroot запутите arch_2.sh находится в корне'
echo 'Переходим в chroot'
arch-chroot /mnt