#!/bin/bash
#Внимание скрипт запускать от обычного пользователя!

  if [ "$(id -u)" -eq 0 ]; then
   echo -e "Запустите скрипт от обычного пользоваля, а не из под \e[1;31mрута!\033[0m"
   exit 0
  else
 echo 'Для установки AUR helpers'
	sudo pacman -Syy git --noconfirm
fi

echo "Какой aur helper будем ставить?" #Обязательно должна стоять группа пакетов base-devel (ставится в первом скрипте вместе с base)
read -p "1 - Yaourt (не поддерживается), 2 - Aurman (не поддерживается), 3 - Yay, 4 - Trizen, 5 - Ничего не ставим: " aur_helper_setting
if [[ $aur_helper_setting == 1 ]]; then
git clone https://aur.archlinux.org/package-query.git
cd package-query
makepkg -si
cd ..
git clone https://aur.archlinux.org/yaourt.git
cd yaourt
makepkg -si
cd ..
rm -rf package-query
rm -rf yaourt
elif [[ $aur_helper_setting == 2 ]]; then
git clone https://aur.archlinux.org/aurman.git
cd aurman
makepkg -si --noconfirm --skippgpcheck
cd ..
rm -rf aurman
echo 'Настройка Aurman'
touch ~/.config/aurman/aurman_config
echo '[miscellaneous]
cache_dir=/tmp/aurman
keyserver=hkp://pgp.mit.edu:11371' > ~/.config/aurman/aurman_config
echo 'Кэш aurman теперь в /tmp/aurman и сервер для ключей назначен (именно только для aurman)'  
elif [[ $aur_helper_setting == 3 ]]; then
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ..
rm -rf yay
elif [[ $aur_helper_setting == 4 ]]; then
git clone https://aur.archlinux.org/trizen.git
cd trizen
makepkg -si --noconfirm
cd ..
rm -rf trizen
elif [[ $aur_helper_setting == 5 ]]; then
echo "AUR helper не будет установлен"
fi

echo 'Очистка от осиротевших пакетов'
if [[ ! -n $(pacman -Qdt) ]]; then
	echo "Пакеты сироты не найдены."
else
	pacman -Rns $(pacman -Qdtq)
fi

echo 'Создаем нужные директории'
sudo pacman -S xdg-user-dirs
xdg-user-dirs-update

echo 'Включаем сетевой экран'
sudo ufw enable
echo 'Ограничиваем траффик SSH'
sudo ufw limit SSH
echo 'Удаляем установочные скрипты'
sudo rm  /arch_*

sudo pacman -Syy 
echo "
###############################################################################
# Установка завершена!
###############################################################################
"

exit 0