#!/bin/bash
#Переменные
#add_modules= #При неиспользовании закоментировать (в содержании mkinitcpio.conf удалить) сделать для того, чтобы было удобно добавлять доп. модули пример добавления add_modules=i915 radeon и т.д
group1=users #Основная группа
group2=log,systemd-journal,wheel,power,daemon #Доп группа
user= #Имя Юзера
namecomp= #Имя компа (хоста)
grub_install_HD=/dev/sd# #На какой Жесткий диск будет установлен grub
time_setting= #Какие часовые пояса см в папки /usr/share/zoneinfo/
#-------------
echo 'Прописываем имя компьютера'
echo $namecomp > /etc/hostname
echo 'Настройка времени'
ln -svf /usr/share/zoneinfo/$time_setting /etc/localtime #Нужно редактировать переменную

echo 'Добавляем русскую локаль системы'
echo -e "en_US.UTF-8 UTF-8\nru_RU.UTF-8 UTF-8" >> /etc/locale.gen

echo 'Обновим текущую локаль системы'
locale-gen

echo 'Указываем язык системы'
echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf

echo 'Вписываем KEYMAP=ru FONT=cyr-sun16'
echo 'KEYMAP=ru' >> /etc/vconsole.conf
echo 'FONT=cyr-sun16' >> /etc/vconsole.conf

echo 'Создаем root пароль'
passwd

echo 'Устанавливаем загрузчик'
pacman -Syy
pacman -S grub --noconfirm
grub-install --target=i386-pc $grub_install_HD

echo 'Обновляем grub.cfg'
grub-mkconfig -o /boot/grub/grub.cfg

echo 'Ставим программу для Wi-fi'
pacman -S dialog wpa_supplicant --noconfirm
echo "Создаем пользователя"
sudo useradd -m -g $group1 -G $group2 -s /bin/bash $user
echo "Задаем пароль для пользователя"
sudo passwd $user
echo 'Устанавливаем SUDO'
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

echo -e "Раскомментируем репозиторий \e[1;36mmultilib\033[0m для работы 32-битных приложений в 64-битной системе."
cat >> /etc/pacman.conf <<EOF
#
# /etc/pacman.conf
#
# See the pacman.conf(5) manpage for option and repository directives

#
# GENERAL OPTIONS
#
[options]
# The following paths are commented out with their default values listed.
# If you wish to use different paths, uncomment and update the paths.
#RootDir     = /
#DBPath      = /var/lib/pacman/
#CacheDir    = /var/cache/pacman/pkg/
#LogFile     = /var/log/pacman.log
#GPGDir      = /etc/pacman.d/gnupg/
#HookDir     = /etc/pacman.d/hooks/
HoldPkg     = pacman glibc
#XferCommand = /usr/bin/curl -L -C - -f -o %o %u
#XferCommand = /usr/bin/wget --passive-ftp -c -O %o %u
#CleanMethod = KeepInstalled
#UseDelta    = 0.7
Architecture = auto

# Pacman won't upgrade packages listed in IgnorePkg and members of IgnoreGroup
#IgnorePkg   =
#IgnoreGroup =

#NoUpgrade   =
#NoExtract   =

# Misc options
#UseSyslog
#Color
#TotalDownload
CheckSpace
#VerbosePkgLists

# By default, pacman accepts packages signed by keys that its local keyring
# trusts (see pacman-key and its man page), as well as unsigned packages.
SigLevel    = Required DatabaseOptional
LocalFileSigLevel = Optional
#RemoteFileSigLevel = Required

# NOTE: You must run "pacman-key --init" before first using pacman; the local
# keyring can then be populated with the keys of all official Arch Linux
# packagers with "pacman-key --populate archlinux".

#
# REPOSITORIES
#   - can be defined here or included from another file
#   - pacman will search repositories in the order defined here
#   - local/custom mirrors can be added here or in separate files
#   - repositories listed first will take precedence when packages
#     have identical names, regardless of version number
#   - URLs will have \$repo replaced by the name of the current repo
#   - URLs will have \$arch replaced by the name of the architecture
#
# Repository entries are of the format:
#       [repo-name]
#       Server = ServerName
#       Include = IncludePath
#
# The header [repo-name] is crucial - it must be present and
# uncommented to enable the repo.
#

# The testing repositories are disabled by default. To enable, uncomment the
# repo name header and Include lines. You can add preferred servers immediately
# after the header, and they will be used before the default mirrors.

#[testing]
#Include = /etc/pacman.d/mirrorlist

[core]
Include = /etc/pacman.d/mirrorlist

[extra]
Include = /etc/pacman.d/mirrorlist

#[community-testing]
#Include = /etc/pacman.d/mirrorlist

[community]
Include = /etc/pacman.d/mirrorlist

# If you want to run 32 bit applications on your x86_64 system,
# enable the multilib repositories as required here.

#[multilib-testing]
#Include = /etc/pacman.d/mirrorlist

[multilib]
Include = /etc/pacman.d/mirrorlist

# An example of a custom package repository.  See the pacman manpage for
# tips on creating your own repositories.
#[custom]
#SigLevel = Optional TrustAll
#Server = file:///home/custompkgs
EOF

echo 'Добавляем подцветку для pacman'
sed -i 's/^#Color/Color/' /etc/pacman.conf #Необязательно
pacman -Syy

echo "Куда устанавливем Arch Linux на виртуальную машину?"
read -p "1 - Да, 0 - Нет: " vm_setting
if [[ $vm_setting == 0 ]]; then
  gui_install="xorg-server xorg-drivers xorg-xinit"
elif [[ $vm_setting == 1 ]]; then
  gui_install="xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils"
fi

echo 'Ставим иксы и драйвера'
pacman -S $gui_install

echo 'Ставим шрифты'
pacman -S ttf-croscore ttf-dejavu ttf-ubuntu-font-family ttf-inconsolata ttf-liberation --noconfirm

echo "Производитель вашей видеокарты?"
read -p "1 - Intel, 2 - AMD/ATI, 3 - Nvidia, 4 - Ставлю на виртуальную машину : " vc_setting
if [[ $vc_setting == 1 ]]; then
  vc=i915
elif [[ $vc_setting == 2 ]]; then
  vc=radeon
elif [[ $vc_setting == 3 ]]; then
  vc=nouveau
elif [[ $vc_setting == 4 ]]; then
  vc="vboxguest vboxsf vboxvideo"
fi

cat >> /etc/mkinitcpio.conf <<EOF
# vim:set ft=sh
# MODULES
# The following modules are loaded before any boot hooks are
# run.  Advanced users may wish to specify all system modules
# in this array.  For instance:
#     MODULES=(piix ide_disk reiserfs)
MODULES=($vc $add_modules)

# BINARIES
# This setting includes any additional binaries a given user may
# wish into the CPIO image.  This is run last, so it may be used to
# override the actual binaries included by a given hook
# BINARIES are dependency parsed, so you may safely ignore libraries
BINARIES=()

# FILES
# This setting is similar to BINARIES above, however, files are added
# as-is and are not parsed in any way.  This is useful for config files.
FILES=()

# HOOKS
# This is the most important setting in this file.  The HOOKS control the
# modules and scripts added to the image, and what happens at boot time.
# Order is important, and it is recommended that you do not change the
# order in which HOOKS are added.  Run 'mkinitcpio -H <hook name>' for
# help on a given hook.
# 'base' is _required_ unless you know precisely what you are doing.
# 'udev' is _required_ in order to automatically load modules
# 'filesystems' is _required_ unless you specify your fs modules in MODULES
# Examples:
##   This setup specifies all modules in the MODULES setting above.
##   No raid, lvm2, or encrypted root is needed.
#    HOOKS=(base)
#
##   This setup will autodetect all modules for your system and should
##   work as a sane default
#    HOOKS=(base udev autodetect block filesystems)
#
##   This setup will generate a 'full' image which supports most systems.
##   No autodetection is done.
#    HOOKS=(base udev block filesystems)
#
##   This setup assembles a pata mdadm array with an encrypted root FS.
##   Note: See 'mkinitcpio -H mdadm' for more information on raid devices.
#    HOOKS=(base udev block mdadm encrypt filesystems)
#
##   This setup loads an lvm2 volume group on a usb device.
#    HOOKS=(base udev block lvm2 filesystems)
#
##   NOTE: If you have /usr on a separate partition, you MUST include the
#    usr, fsck and shutdown hooks.
HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)

# COMPRESSION
# Use this to compress the initramfs image. By default, gzip compression
# is used. Use 'cat' to create an uncompressed image.
#COMPRESSION="gzip"
#COMPRESSION="bzip2"
#COMPRESSION="lzma"
#COMPRESSION="xz"
#COMPRESSION="lzop"
#COMPRESSION="lz4"

# COMPRESSION_OPTIONS
# Additional options for the compressor
#COMPRESSION_OPTIONS=()
EOF

echo "Какое кастомное ядро будем ставить?"
echo -e "\e[1;31mВНИМАНИЕ\033[0m для установки ядер под номером 1,3 будет подключен сторонний репозипорий в pacman.conf"
#Для установки linux-pf можно воспользоваться другим репозиторием:
#[home_post-factum_kernels_Arch]
#Server = https://download.opensuse.org/repositories/home:/post-factum:/kernels/Arch/\$arch'
#Но само ядро немного по-другому называется для любой архитектуры linux-pf-generic
read -p "1 - lunux-pf , 2 - linux-zen, 3 - linux-ck , 4 - linux-lts , 5 - Ничего не буду ставить из выше перечисленного: " kernel_setting
if [[ $kernel_setting == 1 ]]; then
	pacman-key --recv-keys BBFE2FD421597395E4FC8C8DF6C85FEE79D661A4
	pacman-key --lsign-key BBFE2FD421597395E4FC8C8DF6C85FEE79D661A4
	pacman-key --refresh-keys
	echo '[home-thaodan]
	Server = https://thaodan.de/home/bidar/home-thaodan/\$arch' >> /etc/pacman.conf
	pacman -Syy linux-pf linux-pf-preset-default linux-pf-headers --noconfirm
	elif [[ $kernel_setting == 2 ]]; then
	sudo pacman -S linux-zen linux-zen-docs linux-zen-headers --noconfirm
	elif [[ $kernel_setting == 3 ]]; then
		pacman-key -r 5EE46C4C && pacman-key --lsign-key 5EE46C4C
	echo '[repo-ck]
	Server = https://archd.hkno.it/\$repo/os/\$arch
	Server = http://repo-ck.com/\$arch' >> /etc/pacman.conf
	pacman -Syy linux-ck linux-ck-headers --noconfirm
	elif [[ $kernel_setting == 4 ]]; then
	sudo pacman -S linux-lts linux-lts-headers linux-lts-docs --noconfirm
	elif [[ $kernel_setting == 5 ]]; then
	echo 'Хорошо, у Вас только стоковое ядро при следущем вопросе ответе отрицательно'
	fi

echo 'Вы ставили кастомное ядро?'
read -p "1 - Да , 0 - Нет " custom_kernel_answer
if [[ $custom_kernel_answer == 1 ]]; then
echo 'Введите название кастомного ядра. (например linux-ck и т.д)'
read -r custom_kernel
mkinitcpio -p linux
mkinitcpio -p $custom_kernel
elif [[ $custom_kernel_answer == 0 ]]; then
mkinitcpio -p linux
fi

echo 'Очистка от осиротевших пакетов'
if [[ ! -n $(pacman -Qdt) ]]; then
	echo -e "Пакеты сироты \e[1;31mне\033[0m найдены."
else
	pacman -Rns $(pacman -Qdtq)
fi

echo -e "Поздравляем, вы установили \e[1;34mArchLinux\033[0m"
echo  -e "Теперь вам надо ввести \e[1;31mreboot\033[0m, чтобы перезагрузиться и не забудь вынуть диск)"

exit 0

#echo 'После перезагрузки запутите arch_3.sh находится в корне пожеланию запускать'
#echo "Перезагрузка через 10 сек"
#read -p "1..." -t 1
#read -p "2..." -t 1
#read -p "3..." -t 1
#read -p "4..." -t 1
#read -p "5..." -t 1
#read -p "6..." -t 1
#read -p "7..." -t 1
#read -p "8..." -t 1
#read -p "9..." -t 1
#read -p "10..." -t 1
#reboot now